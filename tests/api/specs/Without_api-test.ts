import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { ArticlesController } from '../lib/controllers/articles.controller';
import { UserController } from '../lib/controllers/user.controller';
const auth = new AuthController();
const articles = new ArticlesController();
const user = new UserController();

xdescribe('Articles controller | without hooks', () => {
    let accessToken: string, userId: string;
    let articlesCounter: number;

    it(`login`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'Gjgtyrjcthutq1981');
        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        accessToken = response.body.accessToken;
    });

    it(`get current user details`, async () => {
        let response = await user.getCurrentUser(accessToken);
        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        userId = response.body.id;
    });

    it(`get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);

        console.log(response.body.length);
        articlesCounter = response.body.length;
    });

    it(`add article`, async () => {
        let newArticle = {
            authorId: userId,
            authorName: 'ZorianaQA',
            name: 'My second test article',
            text: 'Hey, this is my second test article. Enjoy!',
        };

        let response = await articles.saveArticle(accessToken, newArticle);
        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);

        articlesCounter += 1;
    });

    it(`get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);

        console.log(response.body.length);
        console.log(articlesCounter);
        expect(response.body.length).to.be.equal(articlesCounter);
    });
});
