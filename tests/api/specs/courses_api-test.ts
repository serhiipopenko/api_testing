import { expect } from 'chai';
import { CoursesController } from '../lib/controllers/courses.controller';
const courses = new CoursesController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Courses controller', () => {
    let courseId: string;

    it(`getAllCourses`, async () => {
        let response = await courses.getAllCourses();

        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time should be less than 5s').to.be.lessThan(5000);
        expect(response.body.length).to.be.greaterThan(1);

        courseId = response.body[18].id;
    });

    it(`getPopularCourses`, async () => {
        let response = await courses.getPopularCourses();

        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time should be less than 5s').to.be.lessThan(5000);
        expect(response.body.length).to.be.greaterThan(1);
    });

    it(`getCourseDetails`, async () => {
        let response = await courses.getAllCourseInfoById(courseId);

        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time should be less than 5s').to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_courseInfo);
    });
});
