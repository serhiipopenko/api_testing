import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

xdescribe('Without test data', () => {
    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: '      '`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', '      ');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: '  Gjgtyrjcthutq1981'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', '  Gjgtyrjcthutq1981');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: 'Gjgtyrjcthutq1981'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'Gjgtyrjcthutq1981');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: 'popenko@hydro.mk.ua'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'popenko@hydro.mk.ua');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: 'Gjgtyrjcthutq1981'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'Gjgtyrjcthutq1981');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: 'admin'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'admin');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });

    it(`login using invalid credentials email: 'popenko@hydro.mk.ua', password: 'Gjgtyrjcthutq1981'`, async () => {
        let response = await auth.authenticateUser('popenko@hydro.mk.ua', 'Gjgtyrjcthutq1981');

        expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
        expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
    });
});
