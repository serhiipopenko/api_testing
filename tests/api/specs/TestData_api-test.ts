import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

xdescribe('Use test data', () => {
    let invalidCredentialsDataSet = [
        { email: 'popenko@hydro.mk.ua', password: '      ' },
        { email: 'popenko@hydro.mk.ua', password: 'Gjgtyrjcthutq1981 ' },
        { email: 'popenko@hydro.mk.ua', password: 'Gjgtyrjcthutq 1981' },
        { email: 'popenko@hydro.mk.ua', password: 'admin' },
        { email: 'popenko@hydro.mk.ua', password: 'popenko@hydro.mk.ua' },
        { email: 'popenko@hydro.mk.ua ', password: 'Gjgtyrjcthutq1981' },
        { email: 'popenko@hydro.mk.ua  ', password: 'Gjgtyrjcthutq1981' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
            expect(response.timings.phases.total, 'Response time should be less than 3s').to.be.lessThan(3000);
        });
    });
});
